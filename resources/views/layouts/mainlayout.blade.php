<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>

    <!-- Custom fonts for this template-->




    @include('panels.styles')

</head>
<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">
    @include('panels.navbar')

    @yield('content')

    <!-- BEGIN: Footer-->
    @include('panels.footer')

    @include('panels.scripts')


