@extends('layouts.mainlayout')
@section('content')

    <?php

      ///  echo $id;
    if($id != "")
    {
        $id = $id;
    }
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://apis-3015.lib.cmu.ac.th/exam/book/'.$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $re = json_decode($response);
    //print_r($re);
    ?>



        <!-- Sidebar -->

        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">


                    <!-- Topbar Search -->


                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->


                        <!-- Nav Item - Alerts -->


                        <!-- Nav Item - Messages -->


                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->


                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"></h1>

                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <?php
//                        foreach($re as $mydata)
//                        {
//                            print_r($mydata['_id']);

                            ?>


                        <div class="col-lg-12">
                            <!-- Default Card Example -->

                            <div class="card mb-4">

                                <div class="card-body">

                                    <?php  $a = count($re->volumeInfo->authors);?>

                                    <img src="<?php echo $re->volumeInfo->imageLinks->smallThumbnail?>">
                                    <h2><?php echo $re->volumeInfo->title?></h2>
                                        <?php
                                        if(isset($re->volumeInfo->authors))
                                        {
                                        if($a > 1)
                                            {
                                                $anss = '';
                                                foreach ($re->volumeInfo->authors as $an)
                                                    {
                                                        $anss = $anss.' '.$an;
                                                    }
                                                ?>

                                        <p><?php echo  'authors'.' '. $anss?></p>

                                        <?php    }else{ ?>

                                        <p><?php echo  'authors'.' '. $re->volumeInfo->authors[0]?></p>

                                       <?php }

                                        ?>

                                    <p><?php echo 'subjects'.' '.$re->subjects?></p>
                                        <?php } ?>
                                        <p><?php echo 'description'.' '.$re->volumeInfo->description?></p>

                                </div>
                            </div>

                        </div>

                        <?php// } ?>









                    </div>




                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->


@endsection

@section('page-scripts')

<script src="../../../app-assets/js/scripts/pages/dashboard-analytics.js">

@endsection
