@extends('layouts.mainlayout')
@section('content')

    <?php

      ///  echo $id;
//    if($id != "")
//    {
//        $id = $id;
//    }
    $curl = curl_init();


    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://cloud-3001.lib.cmu.ac.th/exam/room',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_POSTFIELDS =>'{ }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json','Authorization: Bearer E685'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $re = json_decode($response);
    //print_r($re);
    ?>



        <!-- Sidebar -->

        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">



            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">


                    <!-- Topbar Search -->


                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->


                        <!-- Nav Item - Alerts -->


                        <!-- Nav Item - Messages -->


                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->


                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?php //echo $re->count.' '.'record' ?></h1>

                        <form action="{{url('/')}}" method="post" id="stmp">
                            @csrf


                        </form>

                        <br/>

                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <div class="col-xl-12 col-lg-12">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"> ระบบการจองค้นคว้ากลุ่มสำนักหอสมุด</h6>

                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <form class="user">
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
ห้อง
                                                <select  class="form-control"  name="room" id="room" onchange="checkdate(this.value)">
                                                    <option value="">เลือก</option>
                                                    <?php
                                                    foreach($re as $mydata)
                                                    {

                                                        ?>
                                                    <option value="<?php echo $mydata->room_id?>"><?php echo $mydata->name?></option>

                                                    <?php } ?>

                                                </select>

                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
วันที่

                                                <input  class="form-control datepicker" id="mydatere" data-date-format="mm/dd/yyyy" value="<?php echo date('Y-m-d')?>">
                                            </div>

                                        </div>
                                        <div class="form-group row">

                                            <div class="col-sm-12 mb-3 mb-sm-0">
                                                เลือกเวลาจอง
                                                <div id="dtime">

                                                </div>

                                            </div>




                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>




                    </div>




                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->


@endsection

@section('page-scripts')


      <script>
                function myFunction() {
                    $('#stmp').submit();

                }




                function checkdate(id)
                {
                    var datares = new Array();
                    var html = '';
                    $.ajax({
                        type: "GET",
                        url: "https://cloud-3001.lib.cmu.ac.th/exam/reserve/"+$('#room').val() +"/"+$('#mydatere').val(),
                        data : {


                        },
                        headers: {
                            "Authorization" : "Bearer E685"
                        },
                        dataType: "json",
                        success: function(data) {


                            $.each(data, function (key, val) {
                                // console.log(val.slot_id);
                                datares.push(val.slot_id);
                                //datares = val.slot_id;
                            })
                            $.ajax({
                                type: "GET",
                                url: "https://cloud-3001.lib.cmu.ac.th/exam/slot",

                                headers: {
                                    "Authorization" : "Bearer E685"
                                },
                                dataType: "json",
                                success: function(data) {
                                    console.log(datares);


                                    $.each(data, function (key, val) {

                                        //console.log(datares.includes(val.slot_id));
                                        console.log(datares);

                                        if(datares.includes(val.slot_id) == true)
                                        {
                                            html = html +  '<a href="#" onclick="cancss('+ val.slot_id  +')"><div class="px-3 py-5 bg-gradient-danger text-white">'+val.time_start +' '+val.time_end+'<br/> Not Availble</div></a>';
                                        }else{

                                            html = html +  '<a href="#" onclick="comcss('+ val.slot_id  +')"><div class="px-3 py-5 bg-gradient-success text-white">'+val.time_start +' '+val.time_end+' <br/>  Availble</div></a>';
                                        }



                                    })


                                    $('#dtime').html(html);
                                },
                                failure: function(errMsg) {
                                    alert(errMsg);
                                }
                            });




                        },
                        failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });



                }



                function comcss(bid) {

                    var c = window.confirm("Comfrim to reserve this room");

                    if (c == false) {
                        return;
                    }

                    jQuery.ajax({
                        type : "PUT",
                        url : "https://cloud-3001.lib.cmu.ac.th/exam/reserve/" + bid+"/"+$('#mydatere').val(),
                        headers: {
                            "Authorization" : "Bearer E685"
                        },
                        dataType : "text",
                        cache : false,
                        success : function(data) {

                            //console.log(data);
                                // window.location = "index";
                                location.reload();


                        }

                    });
                }


                function cancss(bid) {

                    var c = window.confirm("Comfrim to cancel ");

                    if (c == false) {
                        return;
                    }

                    jQuery.ajax({
                        type : "DELETE",
                        url : "https://cloud-3001.lib.cmu.ac.th/exam/reserve/" + bid+"/"+$('#mydatere').val(),
                        headers: {
                            "Authorization" : "Bearer E685"
                        },
                        dataType : "text",
                        cache : false,
                        success : function(data) {

                            //console.log(data);
                            // window.location = "index";
                            location.reload();


                        }

                    });
                }


      </script>



@endsection
