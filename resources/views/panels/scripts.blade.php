
    <!-- BEGIN: Vendor JS-->
    <script>
        var assetBaseUrl = "{{ asset('') }}";
    </script>
    <script src="{{asset('asset/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('asset/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('asset/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('asset/js/sb-admin-2.min.js')}}"></script>
    <script src="{{asset('bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

<script>

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',

    });

    </script>
    <!-- BEGIN Vendor JS-->


    <!-- BEGIN: Page Vendor JS-->
    @yield('vendor-scripts')

    <!-- BEGIN: Page JS-->
    @yield('page-scripts')
    <!-- END: Page JS-->



